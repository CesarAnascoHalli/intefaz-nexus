﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.IO;
using System.Text.RegularExpressions;

namespace Nexus
{
    public partial class Form2 : Form
    {
        int mov;
        int movX;
        int movY;

        public Form2()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public string conString = "Data Source=DESKTOP-H14FL7Q;Initial Catalog=DSPEITT;Integrated Security=True";

        private void btnNexusTxt_Click(object sender, EventArgs e)
        {
            generateNexusTxt();
        }

        public void generateNexusTxt()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand mySqlCmd = con.CreateCommand();
            mySqlCmd.CommandText = "EXECUTE NX_EQUIL_FORMAT;";
            mySqlCmd.CommandTimeout = 400;

            try
            {
                con.Open();
                mySqlCmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("PROCESS EXECUTED");

            }
            catch (SqlCeException e)
            {
                ShowErrors(e);
            }
        }

        public static void ShowErrors(SqlCeException e)
        {
            SqlCeErrorCollection errorCollection = e.Errors;

            StringBuilder bld = new StringBuilder();
            Exception inner = e.InnerException;

            if (null != inner)
            {
                MessageBox.Show("Inner Exception: " + inner.ToString());
            }
            // Enumerate the errors to a message box.
            foreach (SqlCeError err in errorCollection)
            {
                bld.Append("\n Error Code: " + err.HResult.ToString("X"));
                bld.Append("\n Message   : " + err.Message);
                bld.Append("\n Minor Err.: " + err.NativeError);
                bld.Append("\n Source    : " + err.Source);

                // Enumerate each numeric parameter for the error.
                foreach (int numPar in err.NumericErrorParameters)
                {
                    if (0 != numPar) bld.Append("\n Num. Par. : " + numPar);
                }

                // Enumerate each string parameter for the error.
                foreach (string errPar in err.ErrorParameters)
                {
                    if (String.Empty != errPar) bld.Append("\n Err. Par. : " + errPar);
                }

                MessageBox.Show(bld.ToString());
                bld.Remove(0, bld.Length);
            }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "INC FILES ONLY (*.INC)|*.INC";

            if (file.ShowDialog() == DialogResult.OK)
            {
                generateFormatTxt(file);
            }

        }

        public List<string> insertItems = new List<string>();
        public void generateFormatTxt(OpenFileDialog ofd)
        {
            txtFileDirectory.Text = ofd.FileName;
            richBoxFile.Text = File.ReadAllText(ofd.FileName);
            List<string> equilItems = new List<string>();
;            
            foreach (string line in File.ReadLines(ofd.FileName))
            {
                equilItems.Add(line);
                if (line == "RSVD                                   -- Generated : Petrel")
                {
                    break;
                }

            }

            equilItems.RemoveRange(0, 9);
            equilItems.RemoveAt(equilItems.Count - 1);
            equilItems.RemoveAt(equilItems.Count - 1);

            foreach( string item in equilItems)
            {
                string newFormattedString = "P_NAME,N_NAME"+ Regex.Replace(item,@"\s+",",");
                richBoxSql.AppendText(newFormattedString.Substring(0, newFormattedString.Length -2) + '\n');
                insertItems.Add(newFormattedString.Substring(0, newFormattedString.Length -2));
            }

         }

        private void btnInsertFileDb_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand mySqlCmd = con.CreateCommand();
            con.Open();
            int counter = 0;

            foreach (string item in insertItems)
            {
                var insertVariables = item.Split(',');
                string EQUIL_P_NAME = insertVariables[0] + "_-" + insertVariables[4] + " - " + insertVariables[8];
                string EQUIL_N_NAME = insertVariables[4] + "-" + insertVariables[8];
                string PVT_N_NAME = insertVariables[8];
                string EQUIL_ORDER = insertVariables[8];
                string P_INIT = insertVariables[3];
                string D_INIT = "-" + insertVariables[2];
                string T_INIT = "0";
                string WOC = "-" + insertVariables[4];
                string PCWOC = insertVariables[9];
                string GOC = "-" + insertVariables[6];
                string PCGOC = insertVariables[10];
                string PSAT = "0";

                string insertT2PV = "INSERT [dbo].[T_P2N_EQUIL] ([EQUIL_P_NAME], [EQUIL_N_NAME], [PVT_N_NAME], [EQUIL_ORDER], [PINIT], [DINIT], [TINIT], [WOC], [PCWOC], [GOC], [PCGOC], [PSAT]) " +
                                           "VALUES (N'" + EQUIL_P_NAME + "',N'" + EQUIL_N_NAME + "',N'" + PVT_N_NAME + "'," + EQUIL_ORDER + "," + P_INIT + "," + D_INIT + "," + T_INIT + "," + WOC + "," + PCWOC + "," + GOC + "," + PCGOC + "," + PSAT + ")";
               
                mySqlCmd.CommandText = insertT2PV;
                System.Console.WriteLine(mySqlCmd.CommandText);
                executeQuery(mySqlCmd);
                counter++;
            }
            con.Close();

            MessageBox.Show(counter.ToString() + " Insert Commands Executed");
        }

        public void executeQuery(SqlCommand query)
        {
            try
            {
                query.ExecuteNonQuery();
                System.Console.WriteLine(query + "--->EXECUTED \n");

            }
            catch (SqlCeException e)
            {
                ShowErrors(e);
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void Form2_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mov = 1;
            movX = e.X;
            movY = e.Y;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if(mov == 1)
            {
                this.SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mov = 0;
        }
    }
}
 