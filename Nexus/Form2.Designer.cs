﻿namespace Nexus
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMinimize = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Label();
            this.btnNexusTxt = new System.Windows.Forms.Button();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.txtFileDirectory = new System.Windows.Forms.TextBox();
            this.btnInsertFileDb = new System.Windows.Forms.Button();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.richBoxSql = new System.Windows.Forms.RichTextBox();
            this.richBoxFile = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMinimize
            // 
            this.btnMinimize.AutoSize = true;
            this.btnMinimize.Font = new System.Drawing.Font("Malgun Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.ForeColor = System.Drawing.Color.Brown;
            this.btnMinimize.Location = new System.Drawing.Point(742, 5);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(20, 25);
            this.btnMinimize.TabIndex = 11;
            this.btnMinimize.Text = "-";
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Brown;
            this.btnClose.Location = new System.Drawing.Point(768, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(20, 21);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "X";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnNexusTxt
            // 
            this.btnNexusTxt.BackColor = System.Drawing.Color.Brown;
            this.btnNexusTxt.FlatAppearance.BorderSize = 0;
            this.btnNexusTxt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed;
            this.btnNexusTxt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnNexusTxt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNexusTxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNexusTxt.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnNexusTxt.Location = new System.Drawing.Point(255, 147);
            this.btnNexusTxt.Name = "btnNexusTxt";
            this.btnNexusTxt.Size = new System.Drawing.Size(248, 36);
            this.btnNexusTxt.TabIndex = 13;
            this.btnNexusTxt.Text = "GENERATE NEXUS TXT";
            this.btnNexusTxt.UseVisualStyleBackColor = false;
            this.btnNexusTxt.Click += new System.EventHandler(this.btnNexusTxt_Click);
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Location = new System.Drawing.Point(1, 33);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 1;
            this.metroTabControl1.Size = new System.Drawing.Size(799, 417);
            this.metroTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Red;
            this.metroTabControl1.TabIndex = 15;
            this.metroTabControl1.UseCustomForeColor = true;
            this.metroTabControl1.UseSelectable = true;
            this.metroTabControl1.UseStyleColors = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.btnNexusTxt);
            this.metroTabPage1.Font = new System.Drawing.Font("Malgun Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(791, 375);
            this.metroTabPage1.Style = MetroFramework.MetroColorStyle.Black;
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "NEXUS";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.txtFileDirectory);
            this.metroTabPage2.Controls.Add(this.btnInsertFileDb);
            this.metroTabPage2.Controls.Add(this.btnOpenFile);
            this.metroTabPage2.Controls.Add(this.richBoxSql);
            this.metroTabPage2.Controls.Add(this.richBoxFile);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(791, 375);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "PETREL";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // txtFileDirectory
            // 
            this.txtFileDirectory.Location = new System.Drawing.Point(130, 15);
            this.txtFileDirectory.Name = "txtFileDirectory";
            this.txtFileDirectory.Size = new System.Drawing.Size(253, 20);
            this.txtFileDirectory.TabIndex = 16;
            // 
            // btnInsertFileDb
            // 
            this.btnInsertFileDb.BackColor = System.Drawing.Color.Brown;
            this.btnInsertFileDb.FlatAppearance.BorderSize = 0;
            this.btnInsertFileDb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed;
            this.btnInsertFileDb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnInsertFileDb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInsertFileDb.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertFileDb.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnInsertFileDb.Location = new System.Drawing.Point(267, 340);
            this.btnInsertFileDb.Name = "btnInsertFileDb";
            this.btnInsertFileDb.Size = new System.Drawing.Size(252, 27);
            this.btnInsertFileDb.TabIndex = 15;
            this.btnInsertFileDb.Text = "INSERT FILE IN DATABASE";
            this.btnInsertFileDb.UseVisualStyleBackColor = false;
            this.btnInsertFileDb.Click += new System.EventHandler(this.btnInsertFileDb_Click);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.BackColor = System.Drawing.Color.Brown;
            this.btnOpenFile.FlatAppearance.BorderSize = 0;
            this.btnOpenFile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed;
            this.btnOpenFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenFile.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFile.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnOpenFile.Location = new System.Drawing.Point(16, 12);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(107, 27);
            this.btnOpenFile.TabIndex = 14;
            this.btnOpenFile.Text = "OPEN FILE";
            this.btnOpenFile.UseVisualStyleBackColor = false;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // richBoxSql
            // 
            this.richBoxSql.Location = new System.Drawing.Point(405, 45);
            this.richBoxSql.Name = "richBoxSql";
            this.richBoxSql.Size = new System.Drawing.Size(367, 289);
            this.richBoxSql.TabIndex = 3;
            this.richBoxSql.Text = "";
            // 
            // richBoxFile
            // 
            this.richBoxFile.Location = new System.Drawing.Point(16, 45);
            this.richBoxFile.Name = "richBoxFile";
            this.richBoxFile.Size = new System.Drawing.Size(367, 289);
            this.richBoxFile.TabIndex = 2;
            this.richBoxFile.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnMinimize);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 31);
            this.panel1.TabIndex = 16;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.metroTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form2_MouseDown);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label btnMinimize;
        private System.Windows.Forms.Label btnClose;
        private System.Windows.Forms.Button btnNexusTxt;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private System.Windows.Forms.Button btnInsertFileDb;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.RichTextBox richBoxSql;
        private System.Windows.Forms.RichTextBox richBoxFile;
        private System.Windows.Forms.TextBox txtFileDirectory;
        private System.Windows.Forms.Panel panel1;
    }
}